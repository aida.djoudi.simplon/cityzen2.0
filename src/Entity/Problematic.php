<?php

namespace App\Entity;



use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\ProblematicRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProblematicRepository::class)
 */
class Problematic
{

    public static $INTERVAL = [
        'jour(s)' => 'day',
        'semaine(s)' => 'week',
        'mois' => 'month'
        
    ];

    public static $PERIOD_VOTE = 'P14D';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="problematics")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=Solution::class, mappedBy="problematic", orphanRemoval=true)
     */
    private $solutions;


    /**
     * @ORM\Column(type="integer")
     */
    private $durationValue;

    /**
     * @ORM\Column(type="string")
     */
    private $durationUnit;

    public function __construct()
    {
        $this->solutions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDurationValue(): ?int
    {
        return $this->durationValue;
    }

    public function setDurationValue(int $durationValue): self
    {
        $this->durationValue = $durationValue;
        return $this;
    }

    public function getDurationUnit(): ?String
    {
        return $this->durationUnit;
    }

    public function setDurationUnit(String $durationUnit): self
    {
        $this->durationUnit = $durationUnit;
        return $this;
    }

    public function getDuration(): String
    {
        return $this->getDurationValue().' '.$this->getDurationUnit();
    }

    public function getFinishedAt(): ?\DateTime
    {
        $interval = \DateInterval::createFromDateString($this->getDuration());
        $dateStart = clone $this->getPublishedAt();
        return $dateStart->add($interval);
    }


    public function getPublishedAt(): ?\DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?\DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getFinishDateVoted(): \DateTime
    {
        $finishedDateMessage = clone $this->getFinishedAt();
        $interval = new \DateInterval(self::$PERIOD_VOTE);
        return $finishedDateMessage->add($interval);
    }

    public function isVotedPeriod(\DateTime $date): Bool
    {
        if($date > $this->getFinishedAt() && $date < $this->getFinishDateVoted()){
            return true;
        }else{
            return false;
        }
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Solution[]
     */
    public function getSolutions(): Collection
    {
        return $this->solutions;
    }

    public function addSolution(Solution $solution): self
    {
        if (!$this->solutions->contains($solution)) {
            $this->solutions[] = $solution;
            $solution->setProblematic($this);
        }

        return $this;
    }

    public function removeSolution(Solution $solution): self
    {
        if ($this->solutions->contains($solution)) {
            $this->solutions->removeElement($solution);
            // set the owning side to null (unless already changed)
            if ($solution->getProblematic() === $this) {
                $solution->setProblematic(null);
            }
        }

        return $this;
    }
}
