<?php 

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

abstract class BaseFixture extends Fixture
{
   

    /**@var ObjectManager */
    public function load(ObjectManager $manager)
    {
        $this->faker = Faker\Factory::create('fr_FR');
        $this->manager = $manager;
        $this->loadData($manager);
    }

    abstract  protected function loadData(ObjectManager $manager);
    

    protected function createMany( string $className, int $count, callable $factory)
    {

        for ($i = 0; $i < $count; $i++)
        {
            $entity = new $className();
            $factory($entity, $i);

            if (null === $entity)
            {
                throw new \LogicException('Did you forget to return the entity object from your callback to BaseFixture::creacteMany()?');
            }
            $this->manager->persist($entity);

            //store for usage later as groupName_#COUNT#
            $this->addReference($className.'_'.$i, $entity);
        }

    }

}