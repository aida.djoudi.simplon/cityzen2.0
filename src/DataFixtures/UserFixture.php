<?php

namespace App\DataFixtures;

use App\Entity\User;


use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends BaseFixture
{

    private $passwordEncoder;
    
    // service for encode password before save in database encodePassword
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

 

    protected function loadData(ObjectManager $manager)
    {

        $this->createMany(User::class, 15, function(User $user, $i){
            $user->setEmail(sprintf('spacebar%d@example.com', $i))
                 ->setPassword($this->passwordEncoder->encodePassword(
                     $user,
                     'new_password'
                 ));
        });
        
        // // création un admin
            $useradmin = new User();

            $useradmin
                ->setEmail('admin@admin.com')
                ->setRoles(['ROLE_ADMIN'])
                ->setPassword($this->passwordEncoder->encodePassword(
                    $useradmin,
                    'admin'
                ))
            ;
            $manager->persist($useradmin);

        

        $manager->flush();
    }
}
