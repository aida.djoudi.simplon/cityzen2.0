<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Problematic;

use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class ProblematicFixture extends BaseFixture implements DependentFixtureInterface
{
    public const UNIT = 'day';

    public const DATE_ARRAY_TEST = [
        '2020-06-05',
        '2020-06-10',
        '2020-06-15',
        '2020-06-20'
    ];

    public function getDependencies()
    {
        return [CustomerFixture::class];
    }

    protected function loadData(ObjectManager $manager)
    {
       
        $this->createMany(Problematic::class, 4, function(Problematic $problematic, $i){

            $problematic
                ->setTitle($this->faker->title)
                ->setCustomer($this->getReference(Customer::class.'_'.$i))
                ->setContent($this->faker->title)
                ->setPublishedAt(new \DateTime(self::DATE_ARRAY_TEST[$i]))
                ->setDurationValue(5)
                ->setDurationUnit(self::UNIT)
            ;
        });
        $manager->flush();
    }
}



