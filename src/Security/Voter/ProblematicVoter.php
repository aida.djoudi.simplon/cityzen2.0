<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class ProblematicVoter extends Voter
{
    const EDIT = 'PROBLEMATIC_EDIT';
    const DELETE = 'PROBLEMATIC_DELETE';

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, [self::EDIT, self::DELETE])
            && $subject instanceof \App\Entity\Problematic;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        $roles = $token->getRoleNames();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        if(in_array('ROLE_MODERATEUR' , $roles, true)){
            return true;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user, $subject, $roles);
                break;
            case self::DELETE:
                return $this->canDelete($user, $subject, $roles);
                break;
        }

        return false;
    }

    private function canEdit($user, $subject, $roles)
    {
        if($user === $subject->getUser() && in_array('ROLE_PROBLEMATIC' , $roles, true)){
            return true;
        }else{
            return false;
        }
    }

    private function canDelete($subject, $roles)
    {
        if(in_array('ROLE_MODERATEUR' , $roles, true) ){
            return true;
        }else{
            return false;
        }
    }
}

