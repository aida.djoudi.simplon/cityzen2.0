<?php

namespace App\Repository;

use App\Entity\Solution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Solution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Solution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Solution[]    findAll()
 * @method Solution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SolutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Solution::class);
    }

    /**
     * @param $value
     * @return Solution[] Returns an array of Solution objects
     */
/*->createQueryBuilder('p')
->select('table1', 'table2')
->join('table1.ceQuetuVeuxJoindre', 'table2')*/

    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')

            ->select('v.solution_id','v.customer_id','s.id','c.id')
            ->from('voted','v')
            ->from('customer','c')
            ->innerJoin('v.solution_id','s.id')
            ->andWhere('v.customer_id = c.id' )
            ->andWhere('s.id = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }


    /*
    public function findOneBySomeField($value): ?Solution
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
